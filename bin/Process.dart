import 'dart:io';
import 'dart:math';

class Process {
String Oper = "";
double sum = 0;
double num = 0;
int count = 0;

Menu (){
  print("Welcome to calculator");
  print("+ Add");
  print("- Subtract");
  print("* Multiply");
  print("/ Divide");
  print("^ Square");
  print("q Exit");
}

checkOperator(String Oper){
  switch (Oper){
    case "+":
      return Add(num);
    case "-":
      return Subtract(num);
    case "*":
      return Multiply(num);
    case "/":
      return Divide(num);
    case "^":
      return Square(num);
    case "q":
      return exit(0);
  }
  return print("Error");
} 

Add(double num){
  sum = sum+num;
  count++;
  Answer();
}

Subtract(double num){
  sum = sum-num;
  count++;
  Answer();
}

Multiply(double num){
  sum = sum*num;
  count++;
  Answer();
}

Divide(double num){
  sum = sum/num;
  count++;
  Answer();
}

Square(double num){
  sum = sum*sum;
  count++;
  Answer();
}

Answer(){
  if(count >= 1){
    print("Answer: " + sum.toString());
  }
  else if (count == 0){
    sum = num;
  }
}
}